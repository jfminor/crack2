#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include "md5.h"

//const int PASS_LEN=100;        // Maximum any password can be
//const int HASH_LEN=33;        // Length of MD5 hash strings

// Given a hash and a plaintext guess, return 1 if
// the hash of the guess matches the given hash.
// That is, return 1 if the guess is correct.
int tryguess(char *hash, char *guess)
{
    // Hash the guess using MD5
    char *guesshash = md5(guess, strlen(guess));
    int g = strcmp(hash, guesshash);
    return g;
    

    // Free any malloc'd memory
    free(hash);
    free(guesshash);
}

// Read in a file and return the array of strings.
// Use the technique we showed in class to expand the
// array as you go.
char **readfile(char *filename)
{
    // Malloc space for entire file
    // Get size of the file
    struct stat st;
    if (stat(filename, &st) == -1)
    {
        fprintf(stderr, "Can't get info about %s\n", filename);
        exit(1);
    }
    int len = st.st_size;
    char *file = malloc(len);

    //Read entire file into memory
    FILE *f = fopen(filename, "r");
    if(!f){
        printf("cant open %s for read\n", filename);
        exit(1);
    }
    fread(file, 1, len, f);
    fclose(f);
    
    
    // Replace \n with \0
    int count = 0;
    for(int i = 0; i < len; i++){
        if(file[i]=='\n'){
            file[i]='\0';
            count++;
        }
    
    }
    
    // Malloc space for array of pointers
    char **line = malloc((count+1)*sizeof(char*));

    //Fill in addresses
    int word = 0;
    line[word] = file;  //first word in the file
    word++;
    for(int i = 1; i < len; i++){
        if(file[i] == '\0' && i+1<len){
            line[word]=&file[i+1];
            word++;
        }
    }
    
    line[word]=NULL;

    //Return address of second array

    return line;
}


int main(int argc, char *argv[])
{
    if (argc < 3) 
    {
        printf("Usage: %s hash_file dict_file\n", argv[0]);
        exit(1);
    }

    // Read the hash file into an array of strings
    char **hashes = readfile("hashes.txt");
    // Read the dictionary file into an array of strings
    char **dict = readfile("rockyou100.txt");

    // For each hash, try every entry in the dictionary.
    // Print the matching dictionary entry.
    // Need two nested loops.
    int i = 0;
    int j = 0;
    while(dict[i]!=NULL){
        while(hashes[j]!=NULL){
            int g = tryguess(hashes[j], dict[i]);
            if(g==0){
                printf("%s\n", dict[i]);
            }
            j++;
        }
        j=0;
        i++;
    }
}
